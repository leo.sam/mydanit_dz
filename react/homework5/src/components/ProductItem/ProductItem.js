import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ItemKontant from '../ItemKontant/ItemKontant';
import Favorites from '../Favorites/Favorites';
import './ProductItem.scss';
import AddCartModal from '../modal/AddCartModal/AddCartModal';
import RemoveCartModal from '../modal/RemoveCartModal/RemoveCartModal';

const ProductItem = ({ product, cart }) => {
  const [expandedCart, setExpandedCart] = useState(false);
  const [remooveCart, setRemooveCart] = useState(false);

  const ButtonHandler = () => {
    setExpandedCart(!expandedCart);
  };

  const RemoveHandler = () => {
    setRemooveCart(!remooveCart);
  };

  return (
    <div className='product__item'>
      <img src={product.img} alt={product.artist.substr(0, 8)} height='200' width='200'></img>
      <ItemKontant product={product} ButtonHandler={ButtonHandler} cart={cart} />

      <Favorites art={product.article} />

      <div className='remove'>
        {cart && (
          <span className='remove__from-cart' onClick={RemoveHandler}>
            &times;
          </span>
        )}
      </div>

      {remooveCart && <RemoveCartModal RemoveHandler={RemoveHandler} product={product} />}

      {expandedCart && <AddCartModal ButtonHandler={ButtonHandler} product={product} />}
    </div>
  );
};

ProductItem.propTypes = {
  product: PropTypes.object.isRequired,
  remove: PropTypes.bool,
  removeFromCart: PropTypes.func,
};

export default ProductItem;
