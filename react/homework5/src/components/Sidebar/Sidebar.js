import React from "react";
import { NavLink } from "react-router-dom";
import "./Sidebar.scss";

// import PropTypes from 'prop-types'

const Sidebar = () => {
  return (
    <div className="menu__list">
      <div className="menu__item">
        <NavLink exact className="menu__link" activeClassName="menu__link-active" to="/products">
          Product
        </NavLink>
      </div>
      <div className="menu__item">
        <NavLink exact className="menu__link" activeClassName="menu__link-active" to="/cart">
          Cart
        </NavLink>
      </div>
      <div className="menu__item">
        <NavLink exact className="menu__link" activeClassName="menu__link-active" to="/favorites">
          Favorites
        </NavLink>
      </div>
    </div>
  );
};

// Sidebar.propTypes = {

// }

export default Sidebar;
