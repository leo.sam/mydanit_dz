import React from "react";
import Button from "../Button/Button";
import "./ItemKontant.scss";

const ItemKontant = ({ product, ButtonHandler, cart }) => {
  return (
    <div className="product__contant">
      <h3 className="product__artist">
        {product.artist}
        <span className="product__by-artist">by Artist</span>
      </h3>
      <h4 className="product__album" style={{ color: product.color }}>
        {product.album}
      </h4>
      <p className="product__text">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      <div className="price__cont">
        <span className="product__price">${product.price}</span>
        {!cart && <Button styles={{ backgroundColor: "#222222", fontSize: "12px" }} text="ADD TO CART" onClick={ButtonHandler} />}
      </div>
    </div>
  );
};

export default ItemKontant;
