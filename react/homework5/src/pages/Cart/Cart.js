import React, { useEffect } from 'react';
import Loader from '../../components/Loader/Loader';
import ProductItem from '../../components/ProductItem/ProductItem';
import './Cart.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getCartSelector } from '../../store/Cart/selectors';
import { pageLoadingSelector } from '../../store/PageLoading/selectors';
import { getCartOperation } from '../../store/Cart/operations';

const Cart = () => {
  const dispatch = useDispatch();
  const productsCart = useSelector(getCartSelector);
  const isLoading = useSelector(pageLoadingSelector);

  useEffect(() => {
    dispatch(getCartOperation());
  }, [dispatch]);

  if (isLoading) {
    return <Loader />;
  }

  const cartList = productsCart.map(p => <ProductItem key={p.article} product={p} cart />);

  return (
    <div className='product__list'>
      {cartList}
      {cartList.length === 0 && <p className='cart-is-empty'>The cart is empty</p>}
    </div>
  );
};

export default Cart;
