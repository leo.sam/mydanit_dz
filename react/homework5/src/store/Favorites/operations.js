import axios from 'axios';
import { saveFavoritesAction } from './actions';
import { pageLoadingAction } from '../PageLoading/actions';

export const getFavoritesOperation = () => (dispatch, getState) => {
  dispatch(pageLoadingAction(true));
  axios.get('../../album.json').then(res => {
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    const productsFavorites = res.data.filter(p => favorites.includes(p.article));
    dispatch(saveFavoritesAction(productsFavorites));
    dispatch(pageLoadingAction(false));
  });
};
