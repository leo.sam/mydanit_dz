import { SET_CART } from './Cart/types';
import { SET_FAVORITES } from './Favorites/types';
import { SET_PAGE_LOADING } from './PageLoading/types';
import { SET_PRODUCTS } from './Products/types';

const initialState = {
  pageLoading: {
    isLoading: true,
  },
  products: {
    data: [],
  },
  cart: {
    data: [],
  },
  favorites: {
    data: [],
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PAGE_LOADING: {
      return { ...state, pageLoading: { ...state.pageLoading, isLoading: action.payload } };
    }
    case SET_PRODUCTS: {
      return { ...state, products: { ...state.products, data: action.payload } };
    }
    case SET_FAVORITES: {
      return { ...state, favorites: { ...state.favorites, data: action.payload } };
    }
    case SET_CART: {
      return { ...state, cart: { ...state.cart, data: action.payload } };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
