import React, { PureComponent } from 'react'
import './Button.scss'

export default class Button extends PureComponent {
    render() {
        const {backgroundColor, text, onClick} = this.props;

        const styles = {
            backgroundColor
        }
        return (
            <button className='button' style={styles} onClick={onClick}>{text}</button>
        )
    }
}
