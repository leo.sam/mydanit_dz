import React, { Component } from 'react';
import './App.css';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expandedFirstModal: false,
      expandedSecondModal: false,
    }
    this.modalRef = React.createRef();
    this.closeRef = React.createRef();
  }
  

  firstButtonHandler = () => {
    const {expandedFirstModal} = this.state;
    this.setState({expandedFirstModal: !expandedFirstModal})
  }

  secondButtonHandler = () => {
    const {expandedSecondModal} = this.state;
    this.setState({expandedSecondModal: !expandedSecondModal})
  }
  closeBtnHandler = (e) => {
    const modal = this.modalRef.current;
    const close = this.closeRef.current;

    if (modal.contains(e.target) && 
      e.target !== close)
    {
      return
    }
    this.setState({expandedFirstModal: false, expandedSecondModal: false})
  }

  render () {
    const {expandedFirstModal, expandedSecondModal} = this.state
    const btnOk = <Button backgroundColor='black' text='Ok' onClick={this.firstButtonHandler}/>
    const btnCancel = <Button backgroundColor='red' text='Cancel' onClick={this.firstButtonHandler}/>
    const btnEdite = <Button backgroundColor='pink' text='Edite' onClick={this.secondButtonHandler}/>

    return (
    <div className="App">
      <Button backgroundColor='green' text='Open first modal' onClick={this.firstButtonHandler}/>
      <Button backgroundColor='blue' text='Open second modal' onClick={this.secondButtonHandler}/>

      {expandedFirstModal && 
          <Modal header='Do you want to delete this file?' closeButton={true}
            text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
            actions={<>{btnOk} {btnCancel}</>} closeBtnHandler={this.closeBtnHandler} modalRef={this.modalRef} closeRef={this.closeRef} />
      }

      {expandedSecondModal &&
        <Modal header='Do you want to edit this file?' closeButton={false}
        text='When you modify this file, the data will be lost forever. Are you sure you want to change it?'
        actions={ <div>{btnEdite}</div> } closeBtnHandler={this.closeBtnHandler} modalRef={this.modalRef} closeRef={this.closeRef} />
      }
    </div>
    )
  };
}

export default App;
