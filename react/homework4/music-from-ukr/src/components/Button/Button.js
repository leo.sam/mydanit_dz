import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';

const Button = ({ styles, text, onClick }) => {
  return (
    <button className='button' style={styles} onClick={onClick}>
      {text}
    </button>
  );
};

Button.propTypes = {
  styles: PropTypes.object,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;
