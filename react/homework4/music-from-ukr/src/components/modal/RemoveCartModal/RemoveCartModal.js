import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { saveCartAction } from '../../../store/Cart/actions';
import { getCartSelector } from '../../../store/Cart/selectors';
import { saveLocal } from '../../../utils/saveLocal';
import { buttonStyle } from '../../../utils/style';
import Button from '../../Button/Button';
import Modal from '../Modal/Modal';

const RemoveCartModal = ({ RemoveHandler, product }) => {
  const dispatch = useDispatch();
  const productsCart = useSelector(getCartSelector);
  const RemoveFromCart = product => {
    if (!productsCart.includes(product)) {
      return;
    }
    let newCart = [];
    productsCart.forEach(p => {
      if (p !== product) {
        newCart.push(p);
      }
    });

    dispatch(saveCartAction(newCart));
    saveLocal(newCart, 'cart');
    RemoveHandler();
  };

  return (
    <Modal
      header={`REMOVE FROM CART?`}
      text={`Do you really want to remove ${product.artist} - ${product.album} from cart?`}
      actions={
        <>
          <Button styles={buttonStyle} text='Ok' onClick={() => RemoveFromCart(product)} />
          <Button styles={buttonStyle} text='Cancel' onClick={RemoveHandler} />
        </>
      }
      buttonHandler={RemoveHandler}
    />
  );
};

export default RemoveCartModal;
