import axios from 'axios';
import { saveCartAction } from './actions';
import { pageLoadingAction } from '../PageLoading/actions';

export const getCartOperation = () => dispatch => {
  dispatch(pageLoadingAction(true));
  axios.get('../../album.json').then(res => {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const productsCart = res.data.filter(p => cart.includes(p.article));
    dispatch(saveCartAction(productsCart));
    dispatch(pageLoadingAction(false));
  });
};
