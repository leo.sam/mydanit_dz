import axios from 'axios';
import { pageLoadingAction } from '../PageLoading/actions';
import { saveProductsAction } from './actions';

export const getProductsOperation = () => (dispatch, getState) => {
  dispatch(pageLoadingAction(true));
  axios.get('../../album.json').then(res => {
    dispatch(saveProductsAction(res.data));
    dispatch(pageLoadingAction(false));
  });
};
