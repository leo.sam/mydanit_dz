import { SET_PAGE_LOADING } from './types';

export const pageLoadingAction = isLoading => ({ type: SET_PAGE_LOADING, payload: isLoading });
