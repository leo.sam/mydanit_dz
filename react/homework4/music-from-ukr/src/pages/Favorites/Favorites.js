import React, { useEffect } from 'react';
import Loader from '../../components/Loader/Loader';
import ProductItem from '../../components/ProductItem/ProductItem';
import './Favorites.scss';
import { useDispatch, useSelector } from 'react-redux';
import { getFavoritesSelector } from '../../store/Favorites/selectors';
import { getFavoritesOperation } from '../../store/Favorites/operations';
import { pageLoadingSelector } from '../../store/PageLoading/selectors';

const Favorites = () => {
  const dispatch = useDispatch();
  const favoritesProduct = useSelector(getFavoritesSelector);
  const isLoading = useSelector(pageLoadingSelector);

  useEffect(() => {
    dispatch(getFavoritesOperation());
  }, [dispatch]);

  if (isLoading) {
    return <Loader />;
  }

  const productList = favoritesProduct.map(p => <ProductItem key={p.article} product={p} />);

  return (
    <div className='product__list'>
      {productList} {productList.length === 0 && <p className='favorites-is-empty'>There are no favorites</p>}
    </div>
  );
};

export default Favorites;
