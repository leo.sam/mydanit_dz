import React, { useState } from "react";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";
import ItemKontant from "../ItemKontant/ItemKontant";
import Favorites from "../Favorites/Favorites";
import "./ProductItem.scss";

const ProductItem = ({ product, cart, removeFromCart }) => {
  const [expandedCart, setExpandedCart] = useState(false);
  const [remooveCart, setRemooveCart] = useState(false);

  const ButtonHandler = () => {
    setExpandedCart(!expandedCart);
  };

  const RemoveHandler = () => {
    setRemooveCart(!remooveCart);
  };

  const AddToCart = () => {
    ButtonHandler();

    const cart = JSON.parse(localStorage.getItem("cart"));

    if (!cart) {
      localStorage.setItem("cart", JSON.stringify([product.article]));
      return;
    }
    if (cart.includes(product.article)) {
      return;
    } else {
      localStorage.setItem("cart", JSON.stringify([...cart, product.article]));
    }
  };

  const buttonStyle = {
    backgroundColor: "#222222",
    fontSize: "16px",
    margin: "20px",
    width: "100px",
  };
  console.log(cart);
  return (
    <div className="product__item">
      <img src={product.img} alt={product.artist.substr(0, 8)} height="200" width="200"></img>
      <ItemKontant product={product} ButtonHandler={ButtonHandler} cart={cart} />

      <Favorites art={product.article} />

      <div className="remove">
        {cart && (
          <span className="remove__from-cart" onClick={RemoveHandler}>
            &times;
          </span>
        )}
      </div>

      {remooveCart && (
        <Modal
          header={`REMOVE FROM CART?`}
          text={`Do you really want to remove ${product.artist} - ${product.album} from cart?`}
          actions={
            <>
              <Button styles={buttonStyle} text="Ok" onClick={() => removeFromCart(product.article)} />
              <Button styles={buttonStyle} text="Cancel" onClick={RemoveHandler} />
            </>
          }
          buttonHandler={RemoveHandler}
        />
      )}

      {expandedCart && (
        <Modal
          header={`ADD TO CART?`}
          text={`Do you really want to add ${product.artist} - ${product.album} to cart?`}
          actions={
            <>
              <Button styles={buttonStyle} text="Ok" onClick={AddToCart} />
              <Button styles={buttonStyle} text="Cancel" onClick={ButtonHandler} />
            </>
          }
          buttonHandler={ButtonHandler}
        />
      )}
    </div>
  );
};

ProductItem.propTypes = {
  product: PropTypes.object.isRequired,
  remove: PropTypes.bool,
  removeFromCart: PropTypes.func,
};

export default ProductItem;
