import React from "react";

const EmptyState = () => {
  return <div className="EmptyState">Your cart is empty. Better rest awaits</div>;
};

export default EmptyState;
