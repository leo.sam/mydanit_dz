import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Products from "../pages/Products/Products";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";

const AppRoutes = () => {
  return (
    <Switch>
      <Redirect exact from="/" to="/products" />
      <Route exact path="/products">
        <Products />
      </Route>
      <Route exact path="/cart">
        <Cart />
      </Route>
      <Route exact path="/favorites">
        <Favorites />
      </Route>
    </Switch>
  );
};

export default AppRoutes;
