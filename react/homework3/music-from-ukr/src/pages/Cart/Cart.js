import React, { useState, useEffect } from "react";
import Loader from "../../components/Loader/Loader";
import ProductItem from "../../components/ProductItem/ProductItem";
import axios from "axios";

const Cart = () => {
  const [isLoading, setisLoading] = useState(true);
  const [product, setProduct] = useState([]);
  let cart = JSON.parse(localStorage.getItem("cart"));

  useEffect(() => {
    axios("../../album.json").then((alb) => {
      setProduct([...alb.data]);
      setisLoading(false);
    });
  }, []);

  const removeFromCart = (article) => {
    let newProduct = [];
    productsCart.forEach((p) => {
      if (p.article !== article) {
        newProduct.push(p);
      }
    });
    setProduct(newProduct);
    console.log(newProduct);

    let newArticleArr = [];
    newProduct.forEach((p) => {
      newArticleArr.push(p.article);
    });

    localStorage.setItem("cart", JSON.stringify(newArticleArr));
  };

  if (isLoading) {
    return <Loader />;
  }

  if (!cart) {
    localStorage.cart = null;
    cart = [];
  }

  const productsCart = product.filter((p) => cart.includes(p.article));

  const productList = productsCart.map((p) => <ProductItem key={p.article} product={p} cart removeFromCart={removeFromCart} />);

  return <div className="product__list">{productList}</div>;
};

export default Cart;
