import React, { useState, useEffect } from "react";
import Loader from "../../components/Loader/Loader";
import ProductItem from "../../components/ProductItem/ProductItem";
import axios from "axios";

const Product = () => {
  const [isLoading, setisLoading] = useState(true);
  const [product, setProduct] = useState([]);

  useEffect(() => {
    axios("../../album.json").then((alb) => {
      setProduct([...alb.data]);
      setisLoading(false);
    });
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  const productList = product.map((p) => <ProductItem key={p.article} product={p} />);

  return <div className="product__list">{productList}</div>;
};

export default Product;
