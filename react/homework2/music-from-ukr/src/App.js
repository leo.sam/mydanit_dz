import React, { Component } from 'react';
import axios from 'axios';
import './App.scss';
import ProductList from './components/ProductList/ProductList';
import Loader from './components/Loader/Loader';


class App extends Component {
  state = {
      isLoading: true,
      product: []
  }


  componentDidMount = () => {
    axios('./album.json')
    .then(alb => {
      this.setState({product: [...alb.data], isLoading: false})
    })
}

  render () {
    const {product, isLoading} = this.state

    if (isLoading) {
      return <Loader />
    }
    return (
    <div className="App">
      <ProductList product={product} buttonHandler={this.ButtonHandler} />
    </div>
    )
  };
}

export default App;
