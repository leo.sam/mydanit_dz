import React, { PureComponent } from 'react'
import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import ModalCart from '../ModalCart/ModalCart';
import PropTypes, { array } from 'prop-types';
import './ProductItem.scss'

class ProductItem extends PureComponent {
    state = {
        expandedCart: false,
        isFavorites: false
    }

    componentDidMount() {
        const {product} = this.props;
        const fav = JSON.parse(localStorage.getItem("favorites"));
        
        if (!fav) {
            return
        }
        if (fav.includes(product.article)) {
            this.setState({isFavorites: true})
        }
    }
    
    FavoritesHandler = () => {
        const { product } = this.props;
        const { isFavorites } = this.state
        const fav = JSON.parse(localStorage.getItem("favorites"));

        if (isFavorites) {
            let newFav = [];
            fav.forEach((f) => {
                if (f !== product.article) {
                    newFav.push(f)
                    return
                }
            })
            localStorage.setItem("favorites", JSON.stringify([...newFav]));
            this.setState({isFavorites: false})
            return
        }

        if (typeof fav !== array) {
            localStorage.setItem("favorites", JSON.stringify([product.article]));
            this.setState({isFavorites: true})
        }

        if (!isFavorites) {
            localStorage.setItem("favorites", JSON.stringify([...fav, product.article]));
            this.setState({isFavorites: true})
            return
        }
    }

    ButtonHandler = () => {
        const {expandedCart} = this.state;
        this.setState({expandedCart: !expandedCart})
    }

    AddToCart = () => {
        const {product} = this.props;
        this.ButtonHandler();

        const cart = JSON.parse(localStorage.getItem("cart"));

        if (!cart) {
            localStorage.setItem("cart", JSON.stringify([product.article]));
            return
        } if (cart.includes(product.article)) {
            return
        } else {
            localStorage.setItem("cart", JSON.stringify([...cart, product.article]));
        }
    }

    render() {
        
        const {product} = this.props;
        const {expandedCart, isFavorites} = this.state;

        const buttonStyle = {
            backgroundColor: '#222222',
            fontSize: '12px',
        }

        return (
            <div className="card__item" >
                <img src={product.img} alt={product.artist.substr(0, 8)} width='200'></img>
                <div className="card__cantant" >
                    <h3 className="card__artist">
                        {product.artist}
                        <span className="card__by-artist" >by Artist</span>
                    </h3>
                    <h4 className="card__album" style={{color: product.color}}>{product.album}</h4>
                    <p className="card__text">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <div className="price__cont">
                        <span  className="card__price">${product.price}</span>
                        <Button styles={buttonStyle} text='ADD TO CART' onClick={this.ButtonHandler}/>
                    </div>
                    <div className="price__favorites">
                        {!isFavorites && <Icon type='star' color='gold' width={25} height={26} onClick={this.FavoritesHandler} />}
                        {isFavorites && <Icon type='star' color='gold' filled width={25} height={26} onClick={this.FavoritesHandler} />}
                    </div>
                    <ModalCart product={product} expandedCart={expandedCart} ButtonHandler={this.ButtonHandler} AddToCart={this.AddToCart} />
                </div>
            </div>
        )
    }
}

ProductItem.propTypes = {
    product: PropTypes.object.isRequired,
}

export default ProductItem