import React, { PureComponent } from 'react'
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';

class ModalCart extends PureComponent {
  render() {
    const { product, expandedCart, ButtonHandler, AddToCart } = this.props

    const buttonStyle = {
      backgroundColor: '#222222',
      fontSize: '16px',
      margin: '20px',
      width: '100px'
  }
    return (
      <div>
        {expandedCart && 
          <Modal header={`ADD TO CART?`} closeButton={true} 
          text={`Do you really want to add ${product.artist} - ${product.album} to cart?`}
          actions={ <>
          <Button styles={buttonStyle} text='Ok' onClick={AddToCart} />
          <Button styles={buttonStyle} text='Cancel' onClick={ButtonHandler} />
          </> } buttonHandler={ButtonHandler} />
        }
      </div>
    )
  }
}

ModalCart.propTypes = {
  product: PropTypes.object.isRequired,
  expandedCart: PropTypes.bool.isRequired,
  ButtonHandler: PropTypes.func,
  AddToCart: PropTypes.func
}

export default ModalCart