import React, { PureComponent } from 'react'
import ProductItem from '../ProductItem/ProductItem';
import PropTypes from 'prop-types';
import './ProductList.scss'

class ProductList extends PureComponent {
    render() {
        const {product} = this.props;

        console.log('Cart', localStorage.getItem('cart'));
        console.log('favorites', localStorage.getItem('favorites'));

        const productCards = product.map(p => <ProductItem key={p.article} product={p} />);
        
        return (
            <div className="card__list">
                {productCards}
            </div>
        )
    }
}

ProductList.propTypes = {
    product: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default ProductList;