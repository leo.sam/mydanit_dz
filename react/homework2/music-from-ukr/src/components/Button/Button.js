import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import './Button.scss'

class Button extends PureComponent {
    render() {
        const {styles, text, onClick} = this.props;

        return (
            <button className='button' style={styles} onClick={onClick}>{text}</button>
        )
    }
}

Button.propTypes = {
    styles: PropTypes.object,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

export default Button