import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import './Modal.scss'

class Modal extends PureComponent {
    constructor(props) {
        super(props);
        this.modalRef = React.createRef();
        this.closeRef = React.createRef();
    }

    closeBtnHandler = (e) => {
        const {buttonHandler} = this.props;
        const modal = this.modalRef.current;
        const close = this.closeRef.current;
        if (modal.contains(e.target) && 
            e.target !== close)
        {
            return
        }
        buttonHandler()
    }

    render() {
        const {header, closeButton, text, actions} = this.props

        return (
            <div onClick={this.closeBtnHandler} className='modal-fade'>
                <div className='modal' ref={this.modalRef}>
                    <div className='modal__header'>
                        <h2 className='modal__title'>{header}</h2>
                        {closeButton && <span  className='modal__close' ref={this.closeRef}>&times;</span>}
                    </div>
                    <div className='modal__body'><p>{text}</p></div>
                    <div className='modal__footer'>{actions}</div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.object
}

export default Modal