const { watch } = require("gulp");
const browserSync = require("./serve").browserSync;
const scriptsTask = require("./scripts");
const stylesTask = require("./styles");

const watcher = () => {
	watch("./index.html", (cb) => {
		browserSync.reload();
		cb();
	});
	watch("./src/js/*.js", (cb) => {
		scriptsTask.scripts();
		browserSync.reload();
		cb();
	});
	watch("./src/scss/main.scss", (cb) => {
		stylesTask.styles();
		browserSync.reload();
		cb();
	});
};

exports.watch = watcher;
