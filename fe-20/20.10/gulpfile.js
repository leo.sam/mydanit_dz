const { src, dest, parallel, series, watch } = require("gulp");
const browserSync = require('browser-sync').create();
const jsMinify = require("gulp-js-minify");

const serve = (cb) => {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 5500,
        open: true,
        browser: "firefox",
    });
    cb();
};

const watcher = () => {
    watch('./index.html', (cb) => {
        browserSync.reload();
        cb();
    });
    watch('./src/js/*.js', (cb) => {
        js();
        browserSync.reload();
        cb();
    });
};

const build = () => {
    js();
};

const js = () => {
    return src("./src/js/*.js").pipe(jsMinify()).pipe(dest("./dest/js/"));
};

function defaultTask(cb) {
  // place code for your default task here
  cb();
};

exports.default = parallel(serve, watcher, build)
exports.serve = serve