"use strict";

// console.log(window);

// console.log(document);

// const bodyStyle = document.body.style;

// bodyStyle.backgroundColor = "green";

// console.log(bodyStyle);
// document.body.childNodes[0].textContent = "first text node";

// const nodes = document.body.childNodes;
// const children = document.body.children;

// nodes.forEach((el, index) => {
//   console.log(`${index} => ${el}`);
// });

// Array.from(children).forEach((el, index) => {
//   console.log(`${index} => ${el}`);
// });

// const paragraf = document.getElementsByTagName("p");
// console.log(paragraf);

// const paragraf2 = document.getElementsByClassName(
//   "text-block"
// );
// console.log(paragraf2);

// const paragraf3 = document.getElementById("text");
// console.log(paragraf3);
// console.log(document.body.children.text);

// const paragraf4 = document.querySelector(".text-block");
// console.log(paragraf4);

// const paragraf5 = document.querySelectorAll(".text-block");
// console.log(paragraf5);

// paragraf3.style.cssText = "font-size: 28px; color: #00f";

// paragraf3.innerText = "<span>text</span>";
// paragraf3.innerHTML = "<span>text</span>";
// paragraf3.insertAdjacentText(
//   "afterBegin",
//   "<span>text</span>"
// );
// paragraf3.insertAdjacentHTML(
//   "afterBegin",
//   "<span>text</span>"
// );
// paragraf3.textContent = "<span>text</span>";

// const strongElement = document.createElement("strong");
// strongElement.textContent = "created in JS";

// paragraf3.append("<strong>tag 1</strong>");
// paragraf3.prepend(strongElement);
// paragraf3.after(strongElement);
// paragraf3.before("<strong>tag 1</strong>");

// const idList = document.getElementById("list");
// console.log(idList.innerText);
// console.log(idList.innerHTML);
// console.log(idList.outerHTML);

// const item = document.getElementsByClassName("list-item");
// console.log(item[0].innerText);
// console.log(item[1].innerHTML);
// console.log(item[2].outerHTML);

// const tag = document.getElementsByTagName("li");
// console.log(tag[0].innerText);
// console.log(tag[1].innerHTML);
// console.log(tag[2].outerHTML);

// const li = document.querySelector("li:nth-child(3)");
// console.log(li.innerText);
// console.log(li.innerHTML);
// console.log(li.outerHTML);

// const liAll = document.querySelectorAll("li");
// console.log(liAll[0].innerText);
// console.log(liAll[1].innerHTML);
// console.log(liAll[2].outerHTML);

// const btnEl = document.querySelector(".remove");
// console.log(btnEl);

// btnEl.remove();
// document.body.prepend(btnEl);

// btnEl.classList.remove("btn");
// btnEl.classList.add("btn-2");
// btnEl.classList.toggle("remove");
// btnEl.classList.replace("btn-2", "btn");
// btnEl.className = "remove btn-3";

const storeEls = document.querySelectorAll(".store li");

for (let i = 0; i < storeEls.length; i++) {
  const last = +storeEls[i].textContent.split(" ")[1];
  if (last === 0) {
    storeEls[i].style.color = "red";
    storeEls[i].style.fontWeight = "600";
    storeEls[i].textContent = storeEls[
      i
    ].textContent.replace("0", "The End");
  }
}
