// const num1 = 3;
// const num2 = 5;

// summ();

// Function declaration

// function summ() {
//   console.log(num1 + num2);
//   console.log(window);
//   console.log(this);
// }

// Function expression

// let result;
// const summ1 = function (num1, num2) {
//   num1 = num1 ** 2;
//   num2 = num2 ** 2;
//   let bbb = num1 + num2;
//   return bbb;
// };

// result = summ1(num1, num2);
// console.log(result);

// result = summ1("2", 7, 2, 5);
// console.log(result);

// function summ(a, b) {
//   let result = a + b;
//   return result;
// }
// console.log(summ(3, 6));

// function argsCounter(a, d, e) {
//   return arguments.length;
// }

// console.log(argsCounter(1, 2, 1, 3));

// function count(numFirst, numLast) {
//   if (numFirst > numLast) {
//     console.log("⛔️ Ошибка! Счёт невозможен.");
//   } else if (numFirst === numLast) {
//     console.log("⛔️ Ошибка! Нечего считать.");
//   } else {
//     console.log("🏁 Отсчёт начат.");
//     for (let i = numFirst; i <= numLast; i++) {
//       console.log(i);
//     }
//     console.log("✅ Отсчёт завершен.");
//   }
// }

// count(2, 10);

function countAdvanced(numFirst, numLast, numThird) {
  if (arguments.length !== 3) {
    console.log("⛔️ Ошибка! Счёт невозможен.");
    return;
  }
  if (
    (numFirst !== null &&
      numFirst !== undefined &&
      numFirst !== "" &&
      !isNaN(numFirst)) ||
    (numLast !== null &&
      numLast !== undefined &&
      numLast !== "" &&
      !isNaN(numLast)) ||
    (numThird !== null &&
      numThird !== undefined &&
      numThird !== "" &&
      !isNaN(numThird))
  )
    return;
  else if (numFirst > numLast) {
    console.log("⛔️ Ошибка! Счёт невозможен.");
    return;
  } else if (numFirst === numLast) {
    console.log("⛔️ Ошибка! Нечего считать.");
    return;
  }
  console.log("🏁 Отсчёт начат.");
  for (let i = numFirst; i <= numLast; i++) {
    if (i % numThird === 0) {
      console.log(i);
    }
  }
  console.log("✅ Отсчёт завершен.");
}

countAdvanced(8, 6, 10);
