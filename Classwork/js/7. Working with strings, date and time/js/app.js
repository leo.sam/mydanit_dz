"use strick";

// const getSumm = (...rest) => {
//   let summ = 0;
//   for (let i = 0; i < rest.length; i++) {
//     summ += rest[i];
//   }
//   return summ;
// };

// console.log(getSumm(3, 4, 5, 6));

// const getMax = (...rest) => {
//   if (rest.length < 2) {
//     return "Error";
//   }
//   for (let i = 0; i < rest.length; i++) {
//     if (isNaN(+rest[i]) || typeof rest[i] !== "number") {
//       return `Error in ${i + 1} argument`;
//     }
//   }
//   return Math.max(...rest);
// };
// console.log(getMax(1, 32, "10", 23));

// const log = (
//   message = "Внимание! Сообщение не указано.",
//   count = 1
// ) => {
//   for (let i = 0; i < count; i++) {
//     console.log(message);
//   }
// };

// log("Привет! просто улыбнись)", 32);

// let storage = [
//   "apple",
//   "water",
//   "banana",
//   "pineapple",
//   "tea",
//   "cheese",
//   "coffee",
// ];

// const replaceItems = (name, list) => {
//   let result = storage.find(function (item, index, array) {
//     return item === name;
//   });
//   if (result === undefined) {
//     return "Error товара нет";
//   }
//   if (!Array.isArray(list)) {
//     return "Error списка нет";
//   }
//   let index = storage.indexOf(name);
//   storage.splice(index, 1, ...list);
// };
// console.log(replaceItems("apple", ["drugs", "whine"]));
// console.log(storage);

const days = {
  ua: [
    "Понеділок",
    "Вівторок",
    "Середа",
    "Четвер",
    "П’ятниця",
    "Субота",
    "Неділя",
  ],
  ru: [
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
    "Воскресенье",
  ],
  en: [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ],
};
function organizer() {
  const langs = Object.keys(days);
  let result;
  let lang;
  do {
    lang = prompt("Choose language: ua, ru, en");
    result = langs.find((item) => item === lang);
  } while (result === undefined);

  days[lang].forEach((el) => {
    console.log(el);
  });
}
organizer();
