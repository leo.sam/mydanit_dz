"use strict";

// for (let index = 0; index < 10; index++) {
//   const div = document.createElement("div");
//   div.textContent = index;
//   const color = `rgb(
//         ${Math.floor(Math.random() * 255)},
//         ${Math.floor(Math.random() * 255)},
//         ${Math.floor(Math.random() * 255)}
//     )`;
// }

function getSquare() {
  let side;
  do {
    side = prompt("enter square`s side in px");
  } while (!side);
  let square = document.createElement("div");
  square.style.cssText = `height:${side}px; width:${side}px; background-color: #450;`;
  square.setAttribute("class", "square");
  document.body.innerHTML = "";
  document.body.append = square;
}

getSquare();
