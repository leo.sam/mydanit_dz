"use strict";

// let num = 0;

// for (let i = 0; i < 99; i += 3.33) {
//   if (i / 3.33 === 5) {
//     console.log("На " + i + " шаге число будет " + num);
//     let num1 = num;
//   }
//   console.log("Номер итерации " + i);
//   console.log("Результат: " + ++num);
// }

// while (num < 100) {
//   console.log(num);
//   num += 3;
// }

// do {
//   console.log("do while");
// } while (false);

// for (let i = 0; i <= 300; i++) {
//   if (i % 2 !== 0 && i % 5 !== 0) {
//     console.log(i);
//   }
// }

// Задача 2

// let condition, num1, num2;

// do {
//   num1 = prompt("Введите число 1");
//   num2 = prompt("Введите число 2");

//   condition =
//     !isNaN(+num1) &&
//     num1 !== null &&
//     num1 !== undefined &&
//     num1 !== "" &&
//     !isNaN(+num2) &&
//     num2 !== null &&
//     num2 !== undefined &&
//     num2 !== "";

//   if (condition) {
//     alert(
//       `Поздравляем! Введённые Вами числа ${num1} и ${num2}`
//     );
//   }
// } while (!condition);

// Задача 3

// do {
//   let name = prompt("Your name");
//   let lastName = prompt("Your lastname");
//   let year = +prompt("Your year");

//   if (
//     name !== "" &&
//     lastName !== "" &&
//     year > 1910 &&
//     year <= 2020
//   ) {
//     alert(
//       `Добро пожаловать! Родившийся в ${year} ${name} ${lastName}.`
//     );
//     break;
//   }
// } while (true);
let condition;
let operations;

do {
  let num1 = prompt("Enter number 1");
  let num2 = prompt("Enter number 2");
  let operation = prompt("Enter operation");

  condition =
    !isNaN(+num1) &&
    num1 !== null &&
    num1 !== undefined &&
    num1 !== "" &&
    !isNaN(+num2) &&
    num2 !== null &&
    num2 !== undefined &&
    num2 !== "";

  operations =
    operation === "+" ||
    operation === "-" ||
    operation === "*" ||
    operation === "/" ||
    operation === "**";

  if (condition && operations) {
    switch (operation) {
      case "+":
        console.log(
          `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
            +num1 + +num2
          }`
        );
        break;
      case "-":
        console.log(
          `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
            num1 - num2
          }`
        );
        break;
      case "*":
        console.log(
          `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
            num1 * num2
          }`
        );
        break;
      case "/":
        console.log(
          `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
            num1 / num2
          }`
        );
        break;
      case "**":
        console.log(
          `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
            num1 ** num2
          }`
        );
        break;
      default:
        break;
    }
  }
} while (!condition && !operation);
