"use strict";

// window.addEventListener("", function (event) {

//     const clickHandler = () => {
//         console.log("click");
//     };

//     const onclickBtn = () => {
//         console.log("click 2");
//     };

//     const btn = document.getElementById("click-me");

//     // btn.onclick = clickHandler;
//     // btn.onclick = onclickBtn;
//     // console.log(btn);

//     // btn.addEventListener("mouseleave", clickHandler);
//     // btn.addEventListener("click", onclickBtn);

//     btn.addEventListener("click", function (event) {
//         console.log(event);
//         console.log(this);
//         //   clickHandler();
//         //   onclickBtn();
//     });

//     // const btnMouse = document.getElementById("btn-mouse");

//     // btnMouse.addEventListener("mouseleave", clickHandler);
// }

const root = document.getElementById("root");

// const fragmentHTML = document.createDocumentFragment();

// const clickHandler = (evt) => {
//   const element = evt.target;
//   // element.style.backgroundColor = "yellow";
//   alert("li checked " + element.textContent);
//   element.removeEventListener("click", clickHandler);
// };

// for (let i = 0; i < 10; i++) {
//   const li = document.createElement("li");
//   li.textContent = i;

//   li.addEventListener("click", clickHandler);
//   fragmentHTML.prepend(li);
// }

// const list = document.createElement("ul");
// list.append(fragmentHTML);
// root.prepend(list);

const phrase = "Добро пожаловать!";

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}

const title = document.createElement("h1");
title.textContent = phrase;

const btn = document.createElement("button");
btn.textContent = "Раскрасить";

btn.addEventListener("click", function () {
  const newArr = phrase.split("").map((item) => {
    const span = document.createElement("span");
    span.textContent = item;
    span.style.color = getRandomColor();
    return span;
  });
  //   console.log(newArr);
  const fragment = document.createDocumentFragment();
  //   newArr.forEach((el) => {
  //     fragment.append(el);
  //   });
  fragment.append(...newArr);
  title.innerHTML = "";
  title.prepend(fragment);
});

root.append(title);
root.append(btn);
