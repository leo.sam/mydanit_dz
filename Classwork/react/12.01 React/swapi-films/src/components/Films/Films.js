import React, { PureComponent } from 'react'

class Films extends PureComponent {
    state = {
    expanded: false
  }

  expandFilm = () => {
    this.setState({ expanded: true })
  }

  render() {
    const { film } = this.props;
    const { expanded } = this.state;

    return (
      <li>
        <div>
          {film.name}
          {!expanded && <button onClick={this.expandFilm}>Детальнее</button>}
        </div>
        {expanded && <FilmDetails film={film} />}
      </li>
    )
  }
}

export default Films;