import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Loader from './components/Loader/Loader';
import Films from './components/Films/Films';

class App extends Component {
  state = {
    films: [],
    isLoading: false
  }

  componentDidMount() {
    axios('https://ajax.test-danit.com/api/swapi/films')
      .then(res => this.setState({ films: res.data, isLoading: false }))
  }

  render() {
    const { isLoading, films } = this.state;

    if (isLoading) {
      return <Loader />;
    }

    const filmItems = films.map(f => <Films key={f.id} film={f} />)

    return (
      <div className="App">
        {filmItems}
      </div>
    );
  }
}

export default App;