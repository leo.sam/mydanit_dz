"use stict"

// class Patient {
//   constructor(fullName, birthDate, gender) {
//     this.fullName = fullName;
//     this.birthDate = birthDate;
//     this.gender = gender;
//   }
// }

// class CardioPatient extends Patient {
//   constructor(fullName, birthDate, gender, averagePressure, cardioProblems) {
//     super(fullName, birthDate, gender);
//     this.averagePressure = averagePressure;
//     this.cardioProblems = cardioProblems;
//   }
// }

// class DentalPatient extends Patient {
//     constructor(fullName, birthDate, gender, visitDate, curentTreatment) {
//     super(fullName, birthDate, gender);
//     this.visitDate = visitDate;
//     this.curentTreatment = curentTreatment;
//   }
// }

// ======================TASK2========================== 

const btnReg = document.getElementById("open-register-modal")
const btnLog = document.getElementById("open-login-modal")


class RegisterModal{
  constructor(id, className) {
    this.id = id;
    this.className = className;
    this.closeBtn = null;
    this.wrapper = this.render();
  }
  render() {
    const wrapper = document.createElement("div");
    wrapper.classList.add(...this.className);
    wrapper.id = this.id;

    const modal = document.createElement("div");
    modal.className = "modal-content";

    const span = document.createElement("span");
    span.className = "close";
    span.innerHTML = "&times;";
    this.closeBtn = span;

    const form = document.createElement("form");
    form.id = "register-form";
    form.setAttribute("action", "#");

    const login = this._createInput("text", "login", "Ваш логин", true);
    const email = this._createInput("email", "email", "Ваш email", true);
    const password = this._createInput("password", "password", "Ваш пароль", true);
    const repeatPassword = this._createInput("password", "repeat-password", "Повторите пароль", true);

    const submit = document.createElement("input");
    submit.setAttribute("type", "submit");
    submit.setAttribute("value", "Регистрация");

    form.append(login, email, password, repeatPassword, submit);
    modal.append(span, form);
    wrapper.append(modal);
    return wrapper;
  }
  _createInput(type, name, placeholder, required) {
    const input = document.createElement("input");
    input.setAttribute("type", type);
    input.setAttribute("name", name);
    input.setAttribute("placeholder", placeholder);
    if (required) {
      input.setAttribute("required", required);
    };
    return input;
  }
  togleClass(className) {
    this.wrapper.classList.toggle(className)
  }
  closeBtnHandler() {
    this.closeBtn.addEventListener("click", () => { this.togleClass("active") });
  }
};

class LoginModal {

};

let root = document.getElementById("root");

let reg = new RegisterModal("reg", ["class1", "class2", "modal"]);
  reg.closeBtnHandler();

btnReg.addEventListener("click", function () {
  root.append(reg.wrapper);
  reg.togleClass("active");
})

btnLog.addEventListener("click", function () { })