"use stict"

// const promise1 = new Promise((resolve, reject) => {
// setTimeout(() => {
//     resolve("foo");
// }, 1000);
//   reject(er)
// })

// promise1.then((value) => {
//   console.log(value);
// }).then(() => {
//   console.log(promise1);
// })


// var isMomHappy = true;

// // Promise
// var willIGetNewPhone = new Promise(
//     function (resolve, reject) {
//         if (isMomHappy) {
//             var phone = {
//                 brand: 'Samsung',
//                 color: 'black'
//             };
//             resolve(phone); // Всё выполнено
//         } else {
//             var reason = new Error('mom is not happy');
//             reject(reason); // reject
//         }

//     }
// );

// var showOff = function (phone) {
// var message = 'Hey friend, I have a new ' +
//   phone.color + ' ' + phone.brand + ' phone';
// return Promise.resolve(message);
// };

// var askMom = function () {
//   willIGetNewPhone
//         .then(showOff)
//         .then(function (fulfilled) {
//             // yay, you got a new phone
//             console.log(fulfilled);
//          // output: { brand: 'Samsung', color: 'black' }
//         })
//         .catch(function (error) {
//             // oops, mom don't buy it
//             console.log(error.message);
//          // output: 'mom is not happy'
//         });
// };

// askMom();


function myAsyncFunction(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = () => resolve(xhr.responseText);
    xhr.onerror = () => reject(xhr.statusText);
    xhr.send();
  });
}