"use strict";

// TASK 1

// У вас есть 2 массива -
// объедините их содержимое
// в один warMachine максимально элегантным способом:

// const armodroms = ["Отравленный кинжал", "Золотой бог", "Гарганто", "Фламберг", "Рыцарь"];
// const airships = ["Золар Ауперкаль", "Казнь", "Вечный голод", "Покровитель"];

// const newArr = [...armodroms, ...airships];
// console.log(newArr);

// Task 2

// напишите функцию getPatientStatus, которая принимает в качестве аргумент рост в см и вес в кг пациента, и возвращает 2 параметра - индекс массы тела и степень ожирения. Степени ожирения при разном значении индекса массы тела:
// - от 10 до 15 - анорексия;
// - от 16 до 25 - норма;
// - от 26 до 30 - лишний вес;
// - от 31 до 35 - I степень;
// - от 36 до 40 - II степень;
// - от 41 и выше - III степень;

//  Используйте лучшие правила создания модульного кода

const getPatientStatus = function (height, weight) {
  let index = (weight / height ** 2);
  let arr = [
    {anorexia: [10, 15]},
    {norma: [16, 25]},
    {extraWeight: [26, 30]},
    {firsLevel: [31, 35]},
    {twoLevel: [36, 40]},
    {threeLevel: [41, 999]},
  ];
  arr.forEach((item, index) => {
    let [key, [min, max]] = Object.entries(item);
    if (index <= max && index >= min) {
      return key
    }
  })
};
let patientStatus = getPatientStatus(180, 70);
