// function constructorImitation(name, age) {
//   return {
//     name,
//     age,
//     parent: {
//       dadName: "Petro",
//       motherName: "Olha",
//     },
//     sayHi(){
//       console.log(`Hello ${this.name}`);
//     },
//   };
// }

// const user = constructorImitation("Uasya", 23)
// console.log(user);


// function User (name, age){
//   this.name = name;
//   this.age = age;
//   this.sayHi = function(){
//     console.log(`Hello ${this.name}`);
//   }
// }

// const user2 = new User("Vasilisa", 26)
// console.log(user2);
// user2.sayHi();
// user.sayHi.call(user2);
// user.sayHi.bind(user2);

// function getParents(){
//   console.log(`${this.dadName}, ${this.motherName}`);
// };
// getParents.apply(user.parent);

// const arr = [1, 2, 3, 4];

// function arrFilter() {
//   return this.filter((el) => el >= 3);
// }
// console.log(arrFilter(arr));

// arr.__proto__.arrFilter = arrFilter;
// console.log(arr.arrFilter());

// const arr2 = [2, 6, 7, 8];

// console.log(arr2.arrFilter());


// function User (name, age){
//   this.name = name;
//   this.age = age;
//   this.sayHi = function(){
//     console.log(`Hello ${this.name}`);
//   }
// }

// let user = new User("Leo", 26)
// console.log(User.prototype === user.__proto__);

// TASK 1

// -  Напишите функцию-конструктор, создающую объект, реализующий такой функционал:
//    у нас на странице есть вопрос. Пр первом клике на него под ним открывается
//    ответ на вопрос. При повтором - ответ прячется. Разметку вы найдете в
//    файле.--> <a href="" class="question">Девиз дома Баратеонов</a>-->

//        <div id="root"></div>

// const questionText = "Девиз дома Баратеонов";
// const questionAnswer = "Нам ярость!";

// const link = document.querySelector('.question')

// function Questions(link, questionAnswer) {
//   this.flag = false;
//   this.checkFlag = function () {
//     this.flag = !this.flag
//   }
//   this.answerToggle = function () {
//     link.addEventListener('click', function (e) {
//       e.preventDefault();
//       if (this.flag) {
//         answer.remove();
//       }
//         else {
//           let answer = document.createElement('p');
//           answer.textContent = questionAnswer;
//           let root = document.getElementById('root');
//           root.append(answer);
//       }
//       this.checkFlag.call(Questions);
//     });
//   };
// }

// const question = new Questions(link, questionAnswer);
// question.answerToggle();

function StopWatch(time, link) {
  this._time = time;
  this.container = link;
  this.start = function () {
    
  };
  this.stop = function () {
    
  };
  this.reset = function () {
    
  };
  this.setTime = function (time) {
    let status = 'Error';
    if (time > 0 && time % 1 === 0) {
      this._time = time;
      status = 'success';
    };
    return {
      status: status,
    };
  };
  this.getTime = function () {
    
  };
}