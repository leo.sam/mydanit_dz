const { src, dest, parallel, series, watch } = require("gulp");
const del = require('del');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso')
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin')
const browserSync = require('browser-sync').create();

//очистка папки dist
const clean = () => {
	return del(['dist/*'])
}

const styles = () => {
	return src(['./src/scss/normalized.scss',
		'./src/scss/main.scss',])
		//конкатенация всех css файлов в один styles.min.css
		.pipe(concat('styles.min.css'))
		//компиляция scss файлов в css
		.pipe(sass().on('error', sass.logError))
		//добавление вендорных префиксов к CSS свойствам для поддержки последних нескольких версий каждого из браузеров
		.pipe(autoprefixer({overrideBrowserslist: ['last 2 versions'],
			cascade: false
		}))
		//удаление неиспользуемого css-кода
		.pipe(csso({
            restructure: false,
            sourceMap: true,
            debug: true
        }))
		// Сохранение файла в папке dist
		.pipe(dest('./dist/css/'))
		//слежение за изменением файлов
		.pipe(browserSync.stream());
}

const scripts = () => {
	return src(['./src/js/main.js'])
		//конкатенация всех js файлов в один scripts.min.js
		.pipe(concat('scripts.min.js'))
		//минификация js кода
		.pipe(uglify({ toplevel: true }))
		// Сохранение файла в папке dist
		.pipe(dest('./dist/js/'))
		//слежение за изменением файлов
	.pipe(browserSync.stream());
}

const img = () => {
	return src('src/img/*')
	// оптимизация картинок
        .pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.mozjpeg({quality: 75, progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({
				plugins: [
					{removeViewBox: true},
					{cleanupIDs: false}
				]
			})
		]))
		//копирование картинок в папку dist/img
		.pipe(dest('dist/img/'))
		//слежение за изменением файлов
		.pipe(browserSync.stream());
}

const serve = () => {
	//запуск локального сервера
	browserSync.init({
		server: {
		baseDir: "./",
		},
		port: 5500,
		open: true,
	});
	
	//Следить за CSS файлами
 	watch('./src/scss/*.scss', styles)
 	//Следить за JS файлами
	watch('./src/js/*.js', scripts)
 	//Следить за Img файлами
 	watch('./src/img/*', img)
  	//При изменении HTML запустить синхронизацию
  	watch("./*.html").on('change', browserSync.reload);
}


build = series(clean, parallel(styles,scripts,img));
dev = series(build, serve);

exports.build = build;
exports.dev = dev;