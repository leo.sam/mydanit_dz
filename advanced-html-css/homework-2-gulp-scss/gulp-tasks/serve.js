
const bs = require('browser-sync').create();

exports.serve = () => {
	bs.init({
        server: {
            baseDir: "./"
		},
		port: 5500,
		open: true
    });
}
exports.bs = bs;