"use strict"

const submitIp = document.getElementById('submit__ip');
const root = document.getElementById('root');
const getIpUrl = 'https://api.ipify.org/?format=json';


// submitIp.addEventListener('click', function() {
//   outByIp(getIpUrl, root);
// })

async function outByIp(getIpUrl, root) {
  try {
    const clientIp = await getClientIp(getIpUrl);
    const clientInfo = await getClientInfo(clientIp);
    renderClientInfo(clientInfo, root);
  } catch (e) {
    console.error(e)
  }

}

async function getClientIp(getIpUrl) {
  try {
    const response = await fetch(getIpUrl);
    const data = await response.json();
    const clientIp = data.ip;
    return clientIp;
  } catch(e) {
    console.error(e)
  }
}

async function getClientInfo() {
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/users', {
        method: 'GET'
    });
    const data = await response.json();
    console.log(data);
  } catch(e) {
    console.error(e)
  }
}

getClientInfo()
// function renderClientInfo({ continent,country,region,city,district }, root) {
//   const clientInfoList = document.createElement('ul');
//   const cont = document.createElement('li');
//   const countr = document.createElement('li');
//   const reg = document.createElement('li');
//   const cit = document.createElement('li');
//   const dist = document.createElement('li');

//   cont.innerHTML = `Ваш континент: ${continent.bold()}`;
//   countr.innerHTML = `Страна в которой Вы находитесь: ${country.bold()}`;
//   reg.innerHTML = `А регион: ${region.bold()}`;
//   cit.innerHTML = `Вы в городе: ${city.bold()}`;
//   dist.innerHTML = `А именно в районе: города ${district.bold()}`;
  
//   clientInfoList.append(cont, countr, reg, cit, dist);
//   root.append(clientInfoList);
// }
