"use strict"

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  };
  set name(name) {
		this._name = name;
  };
	get name() {
		return this._name;
  };
  set age(age) {
		this._age = age;
  };
	get age() {
		return this._age;
  };
  set salary(salary) {
		this._salary = salary;
  };
	get salary() {
		return this._salary;
  };
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(salary) {
		this._salary = salary;
  };
	get salary() {
		return this._salary * 3;
  };
}

let leonid = new Programmer("Leo", 26, 14000, ['Ukrainian', 'Russian', 'English']);
let burningMan = new Programmer("Burning Man", 40, 50000, ['Ukrainian', 'Russian', 'English', 'Deutsch']);


console.log(leonid);
console.log(burningMan);


