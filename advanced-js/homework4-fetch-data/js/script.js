"use strict"

const url = "https://swapi.dev/api/films";
const root = document.getElementById("root");

fetch(url)
	.then((response) => response.json())
  .then((data) => {
    const results = data.results;
    results.sort(({ episode_id: item1 }, { episode_id: item2 }) => item1 - item2);

    const listEpisode = document.createElement("ul");

    for (const { episode_id, title, opening_crawl, characters } of results) {

    const listItem = renderEpisodeItem(episode_id, title, opening_crawl,);
        
    listEpisode.append(listItem);
    listEpisode.className = "episode__list";
    root.append(listEpisode);

    characterIterable(episode_id, characters);
      
    }
	})
  .catch((error) => console.log(error.message));


function renderEpisodeItem(id, title, crawl) {
  const listItem = document.createElement("li");
  const itemId = document.createElement("span");
  const itemTitle = document.createElement("h3");
  const itemCrawl = document.createElement("p");
  const animation = document.createElement("div");

  itemId.textContent = `Эпизод №${id}`;
  itemTitle.innerHTML = `${title.bold()}`;
  itemCrawl.innerHTML = `<b>Описание эпизода:</b><br> ${crawl}`;
  animation.innerHTML = '<span class="one anim-item"></span><span class="two anim-item"></span><span class="three anim-item"></span><span class="four anim-item"></span>';
  animation.classList = "animation__container"

  listItem.append(itemId, itemTitle, itemCrawl, animation);
  listItem.className = `episode__item episode-${id}`;
  return listItem;
}

function characterIterable(id, characters) {
  const li = document.getElementsByClassName(`episode-${id}`);
  const animate = document.getElementsByClassName("animation__container");

  const charList = document.createElement("div");
  charList.classList = "character__container"
  for (const character of characters) {
  const charItem = document.createElement("ul");
  charItem.classList = "character__list";
    fetch(character)
      .then((response) => response.json())
      .then((data) => {
        for (const key in data) {
          const charContant = document.createElement("li")
          charContant.classList = "character__item";
          charContant.innerHTML = `<b>${key}:</b> ${data[key]}<br>`;
          charItem.append(charContant)
        }
      })
      .catch((error) => console.log(error.message));

    charList.append(charItem);
  };
  setTimeout(function(){
    li[0].append(charList);
    animate[0].remove();
  }, 3000);
}
