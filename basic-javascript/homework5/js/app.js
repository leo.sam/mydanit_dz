"use strict";

function createNewUser() {
  this.firstName = prompt("Enter your First name");
  this.lastName = prompt("Enter your Last name");
  this.userAge = prompt(
    "Enter your Birthday `dd.mm.yyyy`",
    "04.09.1994"
  );

  this.getLogin = function () {
    let simpleLogin =
      this.firstName.charAt(0).toLowerCase() +
      this.lastName.toLowerCase();
    return simpleLogin;
  };

  this.getAge = function () {
    let dateNow = new Date();
    let birthdayDate = Date.parse(
      this.userAge.split(".").reverse().join(".")
    );
    this.age = Math.floor(
      (dateNow - birthdayDate) / (24 * 3600 * 365.27 * 1000)
    );
    return this.age;
  };

  this.getPassword = function () {
    this.password =
      this.firstName.charAt(0).toUpperCase() +
      this.lastName.toLowerCase() +
      this.userAge.split(".")[2];
    return this.password;
  };
}

let newUser = new createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
