"use strict";

// let num = prompt(`Enter Your number`);
// for (let i = 0; i <= num; i++) {
//   if (num < 5) {
//     console.log(`Sorry, no numbers`);
//   } else if (i % 5 === 0 && i !== 0) {
//     console.log(i);
//   }
// }

// Optional task 1

// let num;
// do {
//   num = prompt(`Enter Your number`);
// } while (
//   !Number.isInteger(+num) ||
//   isNaN(+num) ||
//   num === null ||
//   num === undefined ||
//   num === "" ||
//   num === "0"
// );

// for (let i = 0; i <= num; i++) {
//   if (num < 5) {
//     console.log(`Sorry, no numbers`);
//   } else if (i % 5 === 0 && i !== 0) {
//     console.log(i);
//   }
// }

// Optional task 2

let m = prompt(`Enter less number M`);
let n = prompt(`Enter more number N`);

while (
  !Number.isInteger(+n) ||
  isNaN(+n) ||
  n === null ||
  n === undefined ||
  n === "" ||
  !Number.isInteger(+m) ||
  isNaN(+m) ||
  m === null ||
  m === undefined ||
  m === "" ||
  n <= m
) {
  alert(`Error`);
  m = prompt(`Enter less number M`);
  n = prompt(`Enter more number N`);
}

nextPrime: for (let i = m; i <= n; i++) {
  for (let j = 2; j < i; j++) {
    // проверить, делится ли число..
    if (i % j === 0) continue nextPrime; // не подходит, берём следующее
  }

  console.log(i); // простое число
}
