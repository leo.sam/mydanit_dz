## Ответ на теоретический вопрос

1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM).

DOM помогает выполнять js-у его главную функцию - взаемодействие с html и css, изменение их свойств и функций, управление событиями. При анализе кода браузер сверху вниз строит DOM-дерево в соответствии с html-тегами, дерево в свою очередь имеет объекты, которые мы можем изменить (желательно до загрузки) способом добавления js-кода.
