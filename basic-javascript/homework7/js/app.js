"use strict";

function listCreator(...list) {
  let mapArray;

  for (let i = 0; i < list.length; i++) {
    if (typeof list[i] === "object") {
      Array.from(list[i]);
      mapArray = `<ul> ${list.map(
        (list) => `<li> ${list}</li>`
      )} </ul>`;
    } else {
      mapArray = `<ul> ${list.map(
        (list) => `<li> ${list}</li>`
      )} </ul>`;
    }
  }
  const container = document.getElementById("container");

  container.innerHTML = mapArray.split(",").join("");
}

listCreator(
  "Hello",
  "JavaScript",
  "Kiev",
  "Lutsk",
  "Dnepr",
  "Bukovel",
  ["1", "2", "4"],
  { dsa: 2, dfd: 4 }
);

// Timer

// let time = 10;

// function timeSec() {
//   if (time === 0) {
//     document.body.innerHTML = "";
//   } else {
//     document.getElementById("timer").innerHTML = time;
//     setTimeout(timeSec, 1000);
//     time--;
//   }
// }

// timeSec();
