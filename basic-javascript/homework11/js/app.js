"use strict";

const btn = document.querySelectorAll(".btn");

document.addEventListener("keydown", function (event) {
  btn.forEach((item) => {
    if (
      event.key.toUpperCase() ===
      item.textContent.toUpperCase()
    ) {
      item.classList.add("active");
      return;
    }
    item.classList.remove("active");
  });
});

// для клавиш без учёта языка

// document.addEventListener("keydown", function (event) {
//   btn.forEach((item) => {
//     if (
//       event.code.toUpperCase() ===
//         `key${item.textContent}`.toUpperCase() ||
//       event.key.toUpperCase() ===
//         item.textContent.toUpperCase()
//     ) {
//       item.classList.add("active");
//       return;
//     }
//     item.classList.remove("active");
//   });
// });
