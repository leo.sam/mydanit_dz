"use strict";

function factorial(num) {
  let result = 1;
  do {
    num = prompt(`Enter number`, num);
  } while (!Number(+num) || isNaN(+num));
  for (let i = 1; i <= num; i++) {
    result *= i;
  }
  return result;
}
console.log(factorial());
