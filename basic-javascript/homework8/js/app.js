"use strict";

const price = document.querySelector(".input-price");
const root = document.getElementById("root");
const incorrect = document.createElement("span");
price.addEventListener(
  "focus",
  (event) => {
    event.target.style.border = "2px solid #046e00";
    incorrect.innerHTML = "";
    incorrect.className = "";
  },
  true
);

price.addEventListener(
  "blur",
  (event) => {
    if (price.value <= 0) {
      event.target.style.border = "2px solid #ff0000";
      incorrect.textContent = `Please enter correct price`;
      incorrect.className = "prise-mass";
      root.prepend(incorrect);
    } else {
      const span = document.createElement("span");
      root.className = "prise-mass";
      event.target.style.border = "";
      event.target.style.color = "#00ff00";

      span.textContent = `Текущая цена: ${price.value} `;

      const btn = document.createElement("button");
      btn.textContent = "x";
      btn.style.cssText =
        "background-color: #ffffff; border-radius: 10px; border: 1px solid #222222;";

      root.prepend(span);
      root.append(btn);

      btn.addEventListener("click", (eve) => {
        root.innerHTML = "";
        root.className = "";
        price.value = "0";
        event.target.style.color = "#000000";
      });
    }
  },
  true
);
