"use strict";

function filterBy(list, type) {
  let userList = [];
  for (let i = 0; i < list.length; i++) {
    if (typeof list[i] !== type) {
      userList.push(list[i]);
    }
  }
  console.log(typeof userList);
  return userList;
}

console.log(
  filterBy(
    [
      undefined,
      null,
      26,
      "12 котов",
      undefined,
      "LeoSam",
      true,
      95,
      false,
    ],
    "undefined"
  )
);
