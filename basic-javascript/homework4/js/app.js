"use strict";

function createNewUser() {
  this.firstName = prompt("Enter your First name");
  this.lastName = prompt("Enter your Last name");
  this.getLogin = function () {
    let simpleLogin =
      this.firstName.charAt(0).toLowerCase() +
      this.lastName.toLowerCase();
    return simpleLogin;
  };
}
let newUser = new createNewUser();
console.log(newUser.getLogin());
