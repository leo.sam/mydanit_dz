## Ответы на теоретические вопросы

1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

Первое различие между var и let заключается в том, что при попытке вывести переменную созданную оператором let до её объявления получаем ошибку, в свою очередь переменная созданая оператором var считаются объявленными с самого начала исполнения функции вне зависимости от того в каком месте функции реально находятся их объявления и поэтому выводит значение undefined. Ещё одно их отличие это область видимости: в let локальная область видимости, в var - глобальная. Переменная const имеет существенное отличие от let: переменную const нельзя переназначить, поэтому const нужно использовать только в тех случаях, когда известно, что переменная не будет изменятся и переназначатся.

2. Почему объявлять переменную через var считается плохим тоном?

Способ объявления переменных var имеет некоторые недостатки поэтому объявления переменных ключевым словом var считается плохим тоном. В отличии от let при выводе переменной перед её объявлением var имеет значение undefined, что усложняет определение ошибок в коде. Ещё одно отличие в области видимости, var имеет глобальную область видимости, это затрудняет например исполльзование одинаковых переменных в функциях.
