"use strict";

const fas = document.querySelectorAll(".fas");

fas.forEach(function (trigger) {
  trigger.addEventListener("click", function () {
    if (trigger.classList.contains("fa-eye")) {
      trigger.classList.remove("fa-eye");
      trigger.classList.add("fa-eye-slash");
      trigger.previousElementSibling.type = "text";
    } else {
      trigger.classList.remove("fa-eye-slash");
      trigger.classList.add("fa-eye");
      trigger.previousElementSibling.type = "password";
    }
  });
});

const enterPass = document.getElementById("enter-pass");
const confirmPass = document.getElementById("confirm-pass");
const btn = document.querySelector(".btn");
let error = document.createElement("spzn");
error.textContent = "Нужно ввести одинаковые значения";
error.style.color = "red";

btn.addEventListener("click", function (event) {
  if (enterPass.value === confirmPass.value) {
    error.remove();
    alert("You are Welcome");
  } else {
    btn.before(error);
  }
  event.preventDefault();
});
