"use strict";

$(document).ready(function(){
    $("#anchor").on("click","a", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });
});

(function ($) {
    $(function () {
    let buttonUp = $('#up');
    let pageHeight = $(window).innerHeight();

    $(window).on('scroll', ()=>{
       
        if ($(this).scrollTop() > pageHeight) {
            $('#back-top').fadeIn();
        } else {
             $('#back-top').fadeOut();
        }
    });
        
        buttonUp.click(function () {
            $('body,html').animate({ scrollTop: 0 }, 500);
            return false;
        })
   
    })
})(jQuery);

$("#slide-pop-post").click(function () {
    $(".container-popular-pots").slideToggle();
});