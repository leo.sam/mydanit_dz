"use strict";

const images = document.querySelectorAll(".image-to-show");
const terminate = document.getElementById("terminate");
const resume = document.getElementById("resume");
let n = 0;

function showImage() {
  if (n + 1 === images.length) {
    images[n].classList.remove("active");
    n = 0;
    images[n].classList.add("active");
    return;
  }
  images[n].classList.remove("active");
  n++;
  images[n].classList.add("active");
}

let timerImage = setInterval(showImage, 10000);

terminate.addEventListener("click", function () {
  clearTimeout(timerImage);
});

resume.addEventListener("click", function () {
  timerImage = setInterval(showImage, 1000);
});
