"use strict";

function mathOperation(num1, num2, operation) {
  do {
    num1 = prompt("Enter number 1", num1);
    num2 = prompt("Enter number 2", num2);
    operation = prompt("Enter operation", operation);
  } while (
    !Number(+num1) ||
    isNaN(+num1) ||
    !Number(+num2) ||
    isNaN(+num2) ||
    (operation !== "+" &&
      operation !== "-" &&
      operation !== "*" &&
      operation !== "/" &&
      operation !== "**")
  );
  switch (operation) {
    case "+":
      console.log(
        `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
          +num1 + +num2
        }`
      );
      break;
    case "-":
      console.log(
        `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
          num1 - num2
        }`
      );
      break;
    case "*":
      console.log(
        `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
          num1 * num2
        }`
      );
      break;
    case "/":
      console.log(
        `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
          num1 / num2
        }`
      );
      break;
    case "*":
    case "**":
      console.log(
        `Над числами ${num1} и ${num2} была произведена операция. Результат: ${
          num1 ** num2
        }`
      );
      break;
  }
}
mathOperation();
