"use strict";

function fib(f0, f1, n) {
  do {
    f0 = +prompt(`Enter start sequence numbers`);
    f1 = +prompt(`Enter end sequence numbers`);
    n = +prompt(`Enter serial number`);
  } while (
    !Number(+f0) ||
    isNaN(+f0) ||
    !Number(+f1) ||
    isNaN(+f1) ||
    !Number(+n) ||
    isNaN(+n)
  );
  if (n > 0) {
    for (let i = 3; i <= n; i++) {
      let f2 = f0 + f1;
      f0 = f1;
      f1 = f2;
    }
  } else {
    for (let i = -3; i >= n; i--) {
      let f2 = -f0 + -f1;
      f0 = f1;
      f1 = f2;
    }
  }
  return f1;
}
console.log(fib());
