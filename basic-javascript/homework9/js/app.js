"use strict";

const triggers = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(
  ".tab-content__item"
);

triggers.forEach(function (trigger) {
  trigger.addEventListener("click", function () {
    const id = this.getAttribute("data-tab"),
      content = document.querySelector(
        '.tab-content__item[data-tab="' + id + '"]'
      ),
      activeTrigger = document.querySelector(
        ".tabs-title.active"
      ),
      activeContent = document.querySelector(
        ".tab-content__item.active"
      );

    activeTrigger.classList.remove("active");
    trigger.classList.add("active");

    activeContent.classList.remove("active");
    content.classList.add("active");
  });
});
