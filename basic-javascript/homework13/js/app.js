const btn = document.getElementById("changeTheme");

const colorThem = document.createElement("link");
colorThem.rel = "stylesheet";
colorThem.href = "./css/color-them.css";

function themeAdd() {
  if (localStorage.getItem("Colormode") !== null) {
    colorThem.remove();
    localStorage.removeItem("Colormode");
  } else {
    document.head.append(colorThem);
    localStorage.setItem("Colormode", "enabled");
  }
}

btn.addEventListener("click", themeAdd);

document.addEventListener("DOMContentLoaded", function () {
  if (localStorage.getItem("Colormode") === "enabled") {
    document.head.append(colorThem);
  }
});
